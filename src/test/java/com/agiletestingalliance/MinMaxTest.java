package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;
//comments
public class MinMaxTest {
    @Test
    public void testgreater() throws Exception {

	final int result= new MinMax().greater(5,10);
	assertEquals("greater", 10, result);
        
    }

@Test
    public void testsmaller() throws Exception {

	final int result= new MinMax().greater(10,5);
	assertEquals("greater", 10, result);
        
    }

   @Test
    public void testbar() throws Exception {

        final String result= new MinMax().bar("test");
        assertEquals("bar", "test", result);	
        
    }

    @Test
    public void testbarempty() throws Exception {

        final String result= new MinMax().bar("");
        assertEquals("bar", "", result);	
        
    }


    
}
